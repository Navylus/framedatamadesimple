import { combineReducers } from 'redux'

import matchup from './components/matchup/reducer'

const reducers = {
  matchup
}

export default combineReducers(reducers)
