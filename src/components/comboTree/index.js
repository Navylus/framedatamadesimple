import React from 'react'
import Tree from 'react-d3-tree'
import axios from 'axios'
import { Dropdown, Header, Button } from 'semantic-ui-react'
import { Redirect } from 'react-router'
import './style/style.css'

class ComboTree extends React.Component {
  constructor(props) {
    super(props)
    const { match } = this.props
    const { params } = match
    this.state = {
      character: params.character
    }
  }

  async componentDidMount() {
    const { character } = this.state
    const response = await axios.get(`https://localhost:3000/movesNormal/${character}`)
    const dataPromise = response.data
    const options = []
    await dataPromise.forEach((move) => {
      options.push({
        key: move.moveName,
        text: move.moveName,
        value: move
      })
    }, this)
    this.setState({
      data: dataPromise,
      startWith: dataPromise[0],
      options
    })
  }

  doGraph(data, initMove) {
    if (!data[0]) {
      return {
        name: initMove.moveName,
        children: [
          {
            name: 'Nothing'
          }
        ]
      }
    }
    if (!data[0].startup) {
      return {
        name: initMove.moveName,
        children: [
          {
            name: 'Nothing'
          }
        ]
      }
    }
    if (parseInt(initMove.onHit, 10) < parseInt(data[0].startup, 10)) {
      return {
        name: initMove.moveName,
        attributes: {
          command: initMove.numCmd
        },
        children: [
          {
            name: 'Nothing'
          }
        ]
      }
    }
    const newTree = [
      {
        name: initMove.moveName,
        attributes: {
          command: initMove.numCmd
        },
        children: [
          {
            name: 'Nothing'
          }
        ]
      }
    ]
    let indexToDrop = 0
    data.forEach((move) => {
      if (move.startup) {
        if (
          parseInt(initMove.onHit, 10) >= parseInt(move.startup, 10)
          && initMove.moveName !== move.moveName
        ) {
          newTree[0].children.push({
            name: move.moveName,
            attributes: {
              command: move.numCmd
            },
            children: [this.doGraph(data.splice(indexToDrop), move)]
          })
        }
      }
      indexToDrop += 1
    }, this)
    return newTree
  }

  render() {
    const {
      character, data, options, startWith, redirect
    } = this.state
    if (redirect) {
      return redirect
    }
    return (
      <div className="comboContainer">
        <Header className="comboHeader" as="h2">{`Combos for ${character}`}</Header>
        <div>
          <Button
            color="violet"
            onClick={() => {
              this.setState({
                redirect: <Redirect to="/" />
              })
            }}
          >
            {'< Home Page'}
          </Button>
          <Dropdown
            placeholder="Select Move to start the combo"
            onChange={(event, { value }) => {
              this.setState({
                startWith: value
              })
            }}
            selection
            options={options}
          />
        </div>
        {data ? (
          <div id="treeWrapper" style={{ width: '100vh', height: '80vh' }}>
            <Tree data={this.doGraph(data, startWith)} translate={{ x: 300, y: 300 }} />
          </div>
        ) : (
          <div />
        )}
        <p className="cp">℗ Navylus - 2019</p>
      </div>
    )
  }
}

export default ComboTree
