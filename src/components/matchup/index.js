import React from 'react'
import SelectBar from './components/selectBar'
import CharacterInfo from './components/character-info'
import CharacterMoves from './components/character-moves'
import Footer from './components/footer'

const matchup = () => (
  <div>
    <SelectBar />
    <div className="containerGlobal">
      <CharacterInfo />
      <CharacterMoves />
    </div>
    <Footer />
  </div>
)

export default matchup
