import actionsType from './actions-type'

export const setPlayers = (player1, player2) => ({
  type: actionsType.SET_PLAYERS,
  player1,
  player2
})

export const setInfosPlayer1 = infosPlayer1 => ({
  type: actionsType.SET_INFOS_P1,
  infosPlayer1
})

export const setInfosPlayer2 = infosPlayer2 => ({
  type: actionsType.SET_INFOS_P2,
  infosPlayer2
})

export const setNormalMovesPlayer1 = normalMovesPlayer1 => ({
  type: actionsType.SET_MOVES_NORMAL_P1,
  normalMovesPlayer1
})

export const setNormalMovesPlayer2 = normalMovesPlayer2 => ({
  type: actionsType.SET_MOVES_NORMAL_P2,
  normalMovesPlayer2
})

export const setNewP1 = newP1 => ({
  type: actionsType.SET_NEW_P1,
  newP1
})
