import { fromJS } from 'immutable'
import initialState from './initial-state'
import actionType from '../actions/actions-type'
import '../actions'

const setPlayers = (state, action) => fromJS(state)
  .setIn(['player1'], action.player1)
  .setIn(['player2'], action.player2)
  .toJS()

const setInfosPlayer1 = (state, action) => fromJS(state)
  .setIn(['infosPlayer1'], action.infosPlayer1)
  .toJS()

const setInfosPlayer2 = (state, action) => fromJS(state)
  .setIn(['infosPlayer2'], action.infosPlayer2)
  .toJS()

const setNormalMovesPlayer1 = (state, action) => fromJS(state)
  .setIn(['normalMovesPlayer1'], action.normalMovesPlayer1)
  .toJS()

const setNormalMovesPlayer2 = (state, action) => fromJS(state)
  .setIn(['normalMovesPlayer2'], action.normalMovesPlayer2)
  .toJS()

const setNewP1 = (state, action) => fromJS(state)
  .setIn(['newP1'], action.newP1)
  .toJS()

const matchup = (state = initialState, action) => {
  switch (action.type) {
    case actionType.SET_PLAYERS:
      return setPlayers(state, action)
    case actionType.SET_INFOS_P1:
      return setInfosPlayer1(state, action)
    case actionType.SET_INFOS_P2:
      return setInfosPlayer2(state, action)
    case actionType.SET_MOVES_NORMAL_P1:
      return setNormalMovesPlayer1(state, action)
    case actionType.SET_MOVES_NORMAL_P2:
      return setNormalMovesPlayer2(state, action)
    case actionType.SET_NEW_P1:
      return setNewP1(state, action)
    default:
      return state
  }
}

export default matchup
