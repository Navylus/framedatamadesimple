import React, { Component } from 'react'
import { connect } from 'react-redux'
import { Dropdown, Button } from 'semantic-ui-react'
import axios from 'axios'
import $ from 'jquery'
import {
  setPlayers,
  setInfosPlayer1,
  setInfosPlayer2,
  setNormalMovesPlayer1,
  setNormalMovesPlayer2
} from '../../actions'
import logoGit from '../../../../images/gitlab.svg'
import logoFMS from '../../../../images/logo.png'

class SelectBar extends Component {
  constructor(props) {
    super(props)
    this.characters = this.getChara()
    this.state = {
      player1: '',
      player2: ''
    }

    const { dispatch } = this.props
    this.dispatch = dispatch
  }

  getChara() {
    const character = []

    axios
      .get('https://localhost:3000/character/listCharacter')
      .then((response) => {
        const { data } = response
        $.each(data, (chara) => {
          const { name } = data[chara]
          character.push({ key: name, value: name, text: name })
        })
      })
      .catch(error => error)
    return character
  }

  setP1(value) {
    this.setState({ player1: value })
  }

  setP2(value) {
    this.setState({ player2: value })
  }

  async setP() {
    const { player1 } = this.state
    const { player2 } = this.state
    this.dispatch(setPlayers(player1, player2))
    if (player1 !== '') {
      const response = await axios.get(`https://localhost:3000/character/${player1}`)
      const res = response.data[0]
      this.dispatch(setInfosPlayer1(res))
      await axios
        .get(`https://localhost:3000/movesNormal/normals/${player1}/false`)
        .then((movesP1) => {
          this.dispatch(setNormalMovesPlayer1(movesP1))
        })
        .catch(error => error)
    }
    if (player2 !== '') {
      const response2 = await axios.get(`https://localhost:3000/character/${player2}`)
      const res2 = response2.data[0]
      this.dispatch(setInfosPlayer2(res2))
      await axios
        .get(`https://localhost:3000/movesNormal/normals/${player2}/false`)
        .then((movesP2) => {
          this.dispatch(setNormalMovesPlayer2(movesP2))
        })
        .catch(error => error)
    }
  }

  render() {
    const { matchup } = this.props
    const { infosPlayer1 } = matchup
    const { player1 } = this.state
    return (
      <div className="selectBox">
        <img src={logoFMS} alt="logo FMS" className="fmsLogo" />
        <div className="selectBorder">
          <Dropdown
            className="select"
            placeholder="Player 1"
            value={player1 === '' ? infosPlayer1.name : player1}
            onChange={(e, { value }) => this.setP1(value)}
            search
            selection
            options={this.characters}
          />
          <Button inverted color="violet" onClick={() => this.setP()}>
            Fight!
          </Button>
          <Dropdown
            className="select"
            placeholder="Player 2"
            onChange={(e, { value }) => this.setP2(value)}
            search
            selection
            options={this.characters}
          />
        </div>
        <a
          href="https://gitlab.com/Navylus/framedatamadesimple"
          target="_blank"
          rel="noopener noreferrer"
        >
          <img src={logoGit} alt="logo GitLab" className="gitLabLogo" />
        </a>
      </div>
    )
  }
}

export default connect(state => state)(SelectBar)
