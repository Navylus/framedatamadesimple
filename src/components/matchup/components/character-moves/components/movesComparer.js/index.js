import React from 'react'
import { connect } from 'react-redux'
import { Popup } from 'semantic-ui-react'
import Slide from 'react-reveal/Slide'
import _ from 'lodash'

class MovesComparer extends React.Component {
  constructor(props) {
    super(props)
    this.state = { fastestMove: 0 }
  }

  componentDidMount() {
    this.getMin()
  }

  componentDidUpdate() {
    this.getMin()
  }

  getMin() {
    // get fastest moove duration
    const { matchup } = this.props
    const { data: moves2 } = matchup.normalMovesPlayer2
    if (moves2) {
      const array = moves2.map(el => parseInt(el.startup, 8)).filter(val => val > 2)
      const newFastestMove = Math.min(...array)
      const { fastestMove } = this.state
      if (newFastestMove !== fastestMove) this.setState({ fastestMove: newFastestMove })
    }
  }

  render() {
    const { stance } = this.props
    const { matchup } = this.props
    const { fastestMove, selected } = this.state
    const { data: moves1 } = matchup.normalMovesPlayer1
    const { player2 } = matchup
    const res = []
    if (!moves1) {
      return <div />
    }
    const moves = _.filter(moves1, x => x.moveMotion.match(stance))
    moves.forEach((move) => {
      let isSafe = 'NaN'
      isSafe = parseInt(move.onBlock, 8) + parseInt(fastestMove, 8) > 0 ? 'Safe' : 'NotSafe'
      res.push(
        <Popup
          key={move.startup + move.active + move.numCmd}
          trigger={(
            <div
              className={`${isSafe} ${move.plnCmd} ${move.moveName === selected ? 'active' : ''}`}
              onClick={() => {
                if (selected === move.moveName) {
                  this.setState({ selected: '' })
                } else {
                  this.setState({ selected: move.moveName })
                }
              }}
            >
              {move.moveName}
              {move.moveName === selected ? (
                <Slide top>
                  <div className={`${isSafe} moveDetail`}>
                    <div>{`How long is it ? ${move.active} Frames`}</div>
                    <div>{`How long to start ? ${move.startup} Frames`}</div>
                    <div>{`How long is the other guy stun ? ${move.active} Frames`}</div>
                  </div>
                </Slide>
              ) : (
                <div />
              )}
            </div>
)}
          basic
          reverted
        >
          <Popup.Content className="popupContent">
            <div>{`On Block : ${move.onBlock}`}</div>
            <div>
              {isSafe === 'Safe'
                ? `Could not be punished by ${player2}`
                : `Worst Than ${player2} ${fastestMove} frames`}
            </div>
            <div>{`Motion : ${move.numCmd}`}</div>
          </Popup.Content>
        </Popup>
      )
    }, this)
    return <div className="moves">{res}</div>
  }
}

export default connect(state => state)(MovesComparer)
