import React from 'react'
import { Button, Header } from 'semantic-ui-react'
import { connect } from 'react-redux'
import ReactLoading from 'react-loading'
import { Redirect } from 'react-router'
import MovesComparer from './components/movesComparer.js'

class CharacterMoves extends React.Component {
  constructor(props) {
    super(props)
    this.state = {
      stance: 'N'
    }
  }

  render() {
    const { matchup } = this.props
    const { stance, redirect } = this.state
    const { data: moves1 } = matchup.normalMovesPlayer1
    const { player2 } = matchup
    if (redirect) {
      this.setState({ redirect: null })
      return redirect
    }
    if (!moves1) {
      return <div />
    }
    if (player2 === '') {
      return (
        <div className="select2ndCharacter">
          <Header as="h2">Please select a second character</Header>
          <ReactLoading type="balls" color="black" height="20%" width="5%" />
          <Button
            onClick={() => {
              this.setState({
                redirect: <Redirect to={`/comboTree/${moves1[0].nameCharacter}`} />
              })
            }}
          >
            My Combo Tree
          </Button>
        </div>
      )
    }
    return (
      <div className="moveContainer">
        <Button.Group vertical>
          <Button
            className="buttonMoves"
            color="grey"
            inverted
            onClick={() => {
              this.setState({ stance: '[^DN]' })
            }}
          >
            Command Attack
          </Button>
          <Button
            className="buttonMoves"
            color="grey"
            inverted
            onClick={() => {
              this.setState({ stance: 'N' })
            }}
          >
            Stand
          </Button>
          <Button
            className="buttonMoves"
            color="grey"
            inverted
            onClick={() => {
              this.setState({ stance: 'D' })
            }}
          >
            Crouch
          </Button>
        </Button.Group>
        <MovesComparer stance={stance} />
        <Button
          className="buttonMoves"
          color="grey"
          inverted
          onClick={() => {
            this.setState({
              redirect: <Redirect to={`/comboTree/${moves1[0].nameCharacter}`} />
            })
          }}
        >
          My Combo Tree
        </Button>
      </div>
    )
  }
}

export default connect(state => state)(CharacterMoves)
