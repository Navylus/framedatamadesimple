import React from 'react'
import axios from 'axios'
import Swal from 'sweetalert2'
import ReactLoading from 'react-loading'
import withReactContent from 'sweetalert2-react-content'
import { Header } from 'semantic-ui-react'
import { connect } from 'react-redux'
import { setInfosPlayer1, setNormalMovesPlayer1 } from '../../actions'

class IntroScreen extends React.Component {
  constructor(props) {
    super(props)
    const { dispatch } = this.props
    this.dispatch = dispatch
    this.state = {
      character: false
    }
  }

  async componentDidMount() {
    this.getChara()
    const promise = await axios.get('https://localhost:3000/charactersDescription')
    const { data } = promise
    this.setState({ description: data })
  }

  async getChara() {
    const character = []

    const promise = await axios.get('https://localhost:3000/character/listCharacter')

    const { data } = promise
    data.forEach((chara) => {
      const { name, phrase, color } = chara
      character.push({ name, phrase, color })
    }, this)
    this.setState({ character })
  }

  characterSelectScreen() {
    const { character, description } = this.state
    const res = []
    const SwalReact = withReactContent(Swal)
    character.forEach((chara) => {
      const { name, phrase, color } = chara
      res.push(
        <div
          className="iconSelect"
          style={{ backgroundColor: color }}
          key={`${color}${name}`}
          onClick={() => {
            SwalReact.fire({
              title: `Why should you play ${name}`,
              text: description[name],
              confirmButtonText: "Let's Fight!"
            }).then(async (result) => {
              if (result.value) {
                const response = await axios.get(`https://localhost:3000/character/${name}`)
                const dataRes = response.data[0]
                this.dispatch(setInfosPlayer1(dataRes))
                await axios
                  .get(`https://localhost:3000/movesNormal/normals/${name}/false`)
                  .then((movesP1) => {
                    this.dispatch(setNormalMovesPlayer1(movesP1))
                  })
                  .catch(error => error)
                Swal.fire({ type: 'success', timer: 500, showConfirmButton: false })
              }
            })
          }}
        >
          <img
            src={`https://localhost:3000/Characters/${name}.png`}
            className="iconImage"
            alt={`${name} ${phrase}`}
          />
        </div>
      )
    }, this)
    return res
  }

  render() {
    const { character } = this.state
    if (!character) {
      return (
        <div className="introContainer">
          <ReactLoading type="balls" color="black" height="20%" width="20%" />
          <div>Loading ...</div>
        </div>
      )
    }
    return (
      <div className="introContainer">
        <Header as="h2">Pick a character</Header>
        <div className="characterSelectScreen">{this.characterSelectScreen()}</div>
        <div className="newText">New to street fighter?</div>
      </div>
    )
  }
}

export default connect(state => state)(IntroScreen)
