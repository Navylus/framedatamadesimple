import React from 'react'
import { connect } from 'react-redux'
import { ProgressBar } from 'react-bootstrap'

class VTrigger extends React.Component {
  render() {
    const { vt1, vt2 } = this.props
    return (
      <div className="vtInfos">
        <div className="vt">
          <ProgressBar
            className="medium"
            min={0}
            max={900}
            label={`${vt1 / 300} barres`}
            now={vt1}
          />
          <p>V-Trigger 1</p>
        </div>
        <div className="vt">
          <ProgressBar
            className="medium"
            min={0}
            max={900}
            label={`${vt2 / 300} barres`}
            now={vt2}
          />
          <p>V-Trigger 2</p>
        </div>
      </div>
    )
  }
}

export default connect(state => state)(VTrigger)
