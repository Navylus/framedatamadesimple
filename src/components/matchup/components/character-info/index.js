import React, { Component } from 'react'
import { connect } from 'react-redux'
import { ProgressBar } from 'react-bootstrap'
import { Header } from 'semantic-ui-react'
import IntroScreen from '../introScreen'
import VTrigger from '../VTrigger'

class CharacterInfo extends Component {
  presentationP1(player, p2Name) {
    const { name, color } = player
    const divStyle = {
      backgroundColor: color
    }

    return (
      <div className="mainCharacterContainer">
        {p2Name ? (
          <div className="mainCharacter" style={divStyle}>
            <div>{name}</div>
            <div className="Vs">VS</div>
            <div className="P2display">{p2Name}</div>
          </div>
        ) : (
          <div className="mainCharacter" style={divStyle}>
            <div>{name}</div>
          </div>
        )}
      </div>
    )
  }

  presentationP2(player) {
    const { name, color } = player
    const divStyle = {
      color,
      fontSize: '45px'
    }
    return (
      <div>
        <div style={divStyle}>{name}</div>
      </div>
    )
  }

  jauge({
    health, vgauge1, vgauge2, phrase
  }) {
    let color = 'medium'
    if (health > 1000) {
      color = 'high'
    } else if (health < 1000) {
      color = 'low'
    }

    return (
      <div className="progressBar">
        <Header as="h2">Life Points</Header>
        <ProgressBar className={`${color}`} min={875} max={1075} label={health} now={health} />
        <VTrigger vt1={vgauge1} vt2={vgauge2} />
        <div className="phrase">{phrase}</div>
      </div>
    )
  }

  render() {
    const { matchup } = this.props
    const { infosPlayer1, infosPlayer2 } = matchup
    const { health } = infosPlayer1
    return (
      <div className="mainCharContainer">
        <div className="mainCharInfos">
          {infosPlayer1.length !== 0 ? (
            this.presentationP1(infosPlayer1, infosPlayer2.name)
          ) : (
            <IntroScreen />
          )}
          {health !== undefined ? this.jauge(infosPlayer1) : ''}
        </div>
      </div>
    )
  }
}

export default connect(state => state)(CharacterInfo)
