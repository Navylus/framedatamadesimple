import React from 'react'

class Footer extends React.Component {
  constructor(props) {
    super(props)
    this.state = {}
  }

  render() {
    return (
      <div className="footerContainer">
        <a
          href="https://www.reddit.com/r/StreetFighter/wiki/v/new_player_guide"
          rel="noopener noreferrer"
          target="_blank"
          alt="Learn the game"
          className="guideLink"
        >
          New player guide
        </a>
        <a
          href="https://fightpedia.fandom.com/wiki/Notation"
          rel="noopener noreferrer"
          target="_blank"
          alt="Learn the game"
          className="guideLink"
        >
          Notation guide
        </a>
      </div>
    )
  }
}
export default Footer
