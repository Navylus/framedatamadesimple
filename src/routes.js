import React, { Component } from 'react'
import { BrowserRouter as Router, Route } from 'react-router-dom'

import Matchup from './components/matchup'
import ComboTree from './components/comboTree'

class Routes extends Component {
  render() {
    return (
      <Router>
        <div>
          <Route path="/" component={Matchup} exact />
          <Route path="/comboTree/:character" component={ComboTree} exact />
        </div>
      </Router>
    )
  }
}

export default Routes
